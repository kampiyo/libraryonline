var EmpleadoCreate = {

    init:function(){
        //Functions
        EmpleadoCreate.select2();
    },

    select2: function(){
        $('#unidad_administrativa_id').select2({
            allowClear: true,
            language: "es",
            placeholder:'Busque y seleccione a un individuo.',
            multiple: false,
            ajax: {
                //url: "https://api.github.com/search/repositories",
                url: "/api/unidades_administrativas",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        query: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (response, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used

                    params.page = params.page || 1;

                    return {
                        results: response.data,
                        pagination: {
                            more: (params.page * 30) < response.total_count
                        }
                    };
                },
                cache: true
            },
            initSelection: function (item, callback) {
                var id = item.val();
                var text = item.data('option');
                var data = { id: id, text: text };
                callback(data);
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 3,
            templateResult: formatUnidadAdministrativa, // omitted for brevity, see the source of this page
            templateSelection: formatUnidadAdministrativaSelection // omitted for brevity, see the source of this page

        }).on("change", function(e) {
            // mostly used event, fired to the original element when the value changes
        });
    },

};

$(document).ready(function() {
    EmpleadoCreate.init();
});

function formatUnidadAdministrativa(criminal) {
    if (criminal.loading) return criminal.text;

    var markup = "<div>" + criminal.first_name + " | " + criminal.last_name + " | " + criminal.alias + "</div>";

    return markup;
}

function formatUnidadAdministrativaSelection(criminal) {

    var label;

    if (criminal.first_name) {
        label = criminal.first_name + " | " + criminal.last_name + " | " + criminal.alias;
    }
    else{
        label = criminal.text
    }

    return label
}