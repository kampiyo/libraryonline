@section('page_title', 'Inicio')

@extends('layouts.app')

@section('breadcrumb')

<!-- Small boxes (Stat box) -->
<div class="row">
    <div class="col-lg-3 col-xs-3">
        <!-- small box -->
        <div class="small-box bg-blue">
            <div class="inner">
                <h3>{{$books}}</h3>

                <p>Books</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="books" class="small-box-footer">Click <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <!-- ./col -->
</div>
<!-- /.row -->



@endsection

@section('content')

@endsection

@section('styles')

@endsection
@section('scripts')

@endsection