@section('page_title', 'Books')

@extends('layouts/app')

@section('breadcrumb')
    <h1>
        Books <a href="/book/create" class="btn btn-lg btn-success" >Register new book</a>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Books</li>
    </ol>
@endsection

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="roles_table" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Author</th>
                            <th>Published_date</th>
                            <th>Category</th>
                            <th>Is available</th>
                            <th>Details</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($books as $book)
                            <tr>
                                <td>{!! $book->id !!}</td>
                                <td>{!! $book->name !!}</td>
                                <td>{!! $book->author !!}</td>
                                <td>{!! $book->published_date !!}</td>
                                <td>{!! $book->category->name !!}

                                </td>
                                <td>{!! $book->is_available !!}
                                    <button
                                            type="button"
                                            class="btn btn-primary btn-lg"
                                            data-toggle="modal"
                                            data-target="#favoritesModal">
                                        Rent book
                                    </button>
                                </td>

                                <td>
                                    <a href="/book/{!! $book->id !!}" class="btn btn-sm btn-info">details</a>
                                </td>
                                <td><a href="/book/{!! $book->id !!}/edit" class="btn btn-sm btn-warning">Edit</a></td>
                                <td> <a>
                                        {{ Form::open(['method' => 'DELETE', 'route' => ['book.destroy', $book->id]]) }}
                                        {{ Form::submit('Delete', ['class' => 'btn btn-danger']) }}
                                        {{ Form::close() }}
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Author</th>
                            <th>Published_date</th>
                            <th>Category</th>
                            <th>Is available</th>
                            <th>Details</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

@endsection

@section('styles')

    {!! Html::style('/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css',array('media'=>'screen,projection')) !!}

@endsection

@section('scripts')

    {!! Html::script(asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")) !!}
    {!! Html::script(asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")) !!}

    {!! Html::script(asset("/js/app/index_roles.js")) !!}

@endsection