
@extends('layouts.app')
@section('content')

    <hr>
    <div class="container-fluid span6">
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Book details</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->


                        {!! Form::model($book,array('url' => '/book/'.$book->id, 'method' => 'put')) !!}

                        <div class="box-body container-fluid  row">
                            <div class="form-group col-md-6">
                                {!! Form::label('name', '* Name:') !!}
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="form-group col-md-6">
                                {!! Form::label('author', '* Author:') !!}
                                {!! Form::text('author', null, ['class' => 'form-control']) !!}
                            </div>

                            <div class="form-group col-md-6">
                                {!! Form::label('published_date', '* Published Date') !!}
                                {!! Form::date('published_date', null, ['class' => 'form-control']) !!}
                            </div>

                            <hr>

                        </div>
                        <!-- /.box-body -->
                        {!! Form::close() !!}
                        <div class="form-group col-md-12">
                            <a href="/">
                                <button class="btn btn-success" id="">

                                    Back

                                </button>
                            </a>
                        </div>
                    </div>

                    <!-- /.box -->
                </div>
            </div>
        </section>
    </div>

@endsection