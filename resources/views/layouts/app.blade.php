<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>LibraryOnline | @yield('page_title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    {!! Html::style('/bower_components/AdminLTE/bootstrap/css/bootstrap.min.css',array('media'=>'screen,projection')) !!}
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />

    @yield('styles')

    {!! Html::style('bower_components/AdminLTE/plugins/jQuery/jquery-2.2.3.min.js',array('media'=>'screen,projection')) !!}
    {!! Html::style('bower_components/AdminLTE/dist/css/skins/_all-skins.css',array('media'=>'screen,projection')) !!}
    {!! Html::style('bower_components/components-font-awesome/css/font-awesome.css',array('media'=>'screen,projection')) !!}
    {!! Html::style('bower_components/AdminLTE/dist/css/ionicons.min.css',array('media'=>'screen,projection')) !!}
    {!! Html::style('bower_components/AdminLTE/dist/css/ionicons.css',array('media'=>'screen,projection')) !!}
    <!-- Theme style -->
    {!! Html::style('/bower_components/AdminLTE/dist/css/AdminLTE.min.css',array('media'=>'screen,projection')) !!}
    {!! Html::style('/bower_components/AdminLTE/dist/css/skins/skin-red.min.css',array('media'=>'screen,projection')) !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <meta name="csrf-token" content="{{ Session::token() }}">
</head>
<body class="skin-green">
<div class="wrapper">

    <!-- Header -->
    @include('/layouts/header')

    <!-- Sidebar -->
    @include('/layouts/sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('breadcrumb')
        </section>

        <!-- Main content -->
        <section class="content">
            @include('partials.flash')
            <!-- Your Page Content Here -->
            @yield('content')
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <!-- Footer -->
    @include('/layouts/footer')

</div><!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
{!! Html::script(asset("/bower_components/AdminLTE/bootstrap/js/bootstrap.min.js")) !!}
{!! Html::script(asset("/bower_components/AdminLTE/dist/js/app.min.js")) !!}
@yield('scripts')

</body>
</html>