{{--<ul class="sidebar-menu">--}}
    {{--<li class="header">MAIN NAVIGATION</li>--}}
    {{--<li class="active treeview">--}}
        {{--<a href="#">--}}
            {{--<i class="fa fa-dashboard"></i> <span>Dashboard</span>--}}
            {{--<span class="pull-right-container">--}}
              {{--<i class="fa fa-angle-left pull-right"></i>--}}
            {{--</span>--}}
        {{--</a>--}}
        {{--<ul class="treeview-menu">--}}
            {{--<li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>--}}
            {{--<li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>--}}
        {{--</ul>--}}
    {{--</li>--}}



<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <!-- Optionally, you can add icons to the links -->
            <li><a href="/"><i class="fa fa-dashboard"></i><span>Home</span></a></li>
            <li><a href="/books"><i class="fa fa-file-text-o"></i><span>Books</span></a></li>

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>